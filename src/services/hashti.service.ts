interface IBcrypt {
  hashSync: (data: any, saltOrRounds: string | number) => string;
  compareSync: (data: any, encrypted: string) => boolean;
}

interface IFs {
  readFileSync: (path: string, encoding: BufferEncoding) => string,
}

export interface IHashtiService {
  create(plainText: string): string;
  check(plainText: string, hashString: string): boolean;
  file(path: string): string;
}

export class HashtiService implements IHashtiService {
  public constructor(
    private readonly bcrypt: IBcrypt,
    private readonly fs: IFs,
  ) { }

  /**
   * Hash text
   * @param plainText Text to hash
   * @return hashed result
   */
  public create(plainText: string): string {
    return this.bcrypt.hashSync(plainText, 10);
  }

  /**
   * Compares text with hash string
   * @param plainText Text to compare
   * @param hashString Hash to compare
   * @return true if compare matches, false if not
   */
  public check(plainText: string, hashString: string): boolean {
    return this.bcrypt.compareSync(plainText, hashString);
  }

  /**
   * Get hash result of file using file path
   * @param path path to file
   * @return hashed result
   */
  public file(path: string): string {
    return this.create(this.fileToString(path));
  }

  /**
   * Compare file with hash
   * @param path path to file
   * @param hashString hash to compare with
   * @return tru if file matches has, false if not
   */
  public checkFile(path: string, hashString: string): boolean {
    return this.check(this.fileToString(path), hashString);
  }

  private fileToString(path: string) {
    return this.fs.readFileSync(path, "utf8");
  }
}
