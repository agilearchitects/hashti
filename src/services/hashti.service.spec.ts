// Lib
import { expect } from "chai";
import { describe, it } from "mocha";
import * as fs from "fs";
import * as bcrypt from "bcrypt";

// Hashti service
import { HashtiService } from "./hashti.service";

// Mock
import * as fsMock from "../mock/fs.mock";

describe("HashtiService", () => {
  it("Should be able to hash text", () => {
    const hashtiService = new HashtiService(bcrypt, fs);
    const hashed = hashtiService.create("hello world");
    expect(hashed).to.have.lengthOf(60);
  });
  it("Should be able to check for match", () => {
    const hashtiService = new HashtiService(bcrypt, fs);
    const message = "Hello World"
    const hashed = hashtiService.create(message);
    const check = hashtiService.check(message, hashed);
    expect(check).to.be.true;
  });

  it("Should be able to hash file", () => {
    const hashtiService = new HashtiService(bcrypt, fsMock as any);
    const hashed = hashtiService.file("./hello.world.ts");
    expect(hashed).to.have.lengthOf(60);
  });

  it("Should be able to check file for match", () => {
    const hashtiService = new HashtiService(bcrypt, fsMock as any);
    const file = "./hello.world.ts"
    const hashed = hashtiService.file(file);
    const check = hashtiService.checkFile(file, hashed);
    expect(check).to.be.true;
  });
});